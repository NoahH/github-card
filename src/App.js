import React, { Component } from "react";
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

function UserData(props){
  const classes = useStyles();
  return(
    <Card className={classes.card}>
      <CardMedia
        className={classes.media}
        image={props.data[0].avatar_url}
      />
      <CardContent>
        <p>Login: {props.data[0].login}</p>
        <p>Id: {props.data[0].id}</p>
        <p>Name: {props.data[0].name}</p>
      </CardContent>
    </Card>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {user: [null], active: false};
    this.onToggle = this.onToggle.bind(this);
  }

  onToggle(){
    if(this.state.user[0] === null){
      fetch("https://api.github.com/users/Greg")
      .then(res => res.json())
      .then(data => {
        this.setState({user: [data], active: true});
        console.log(this.state);
      });
    }
    else{
      this.setState({active: !this.state.active});
    }
  }

  render (){
    return(
      <div>
        <button onClick = {this.onToggle}>CLICK ME</button>
        {this.state.active ? <UserData data = {this.state.user}></UserData> : <p></p> }
      </div>
    );
  };
}

export default App;
